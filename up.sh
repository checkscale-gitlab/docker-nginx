#bin/bash
export NGINX_HOST=gza.test

docker run -m 1G --name nginx-1.19.3 --cpus=".5" -p 80:80 \
-e NGINX_HOST=$NGINX_HOST \
-v "$(pwd)"/html:/usr/share/nginx/html:ro \
-v "$(pwd)"/nginx/nginx.conf:/etc/nginx/nginx.conf:ro \
-v "$(pwd)"/nginx/templates/:/etc/nginx/templates/:ro \
-d nginx:1.19.3-alpine
